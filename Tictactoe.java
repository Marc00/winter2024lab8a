import java.util.Scanner;

public class Tictactoe{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		System.out.println("\n	   Welcome to TicTacToe!!\n");
		
		Board board = new Board();
		
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		
		while(!gameOver){
			
			System.out.println("*****************************************\n");
			
			System.out.println(board);
			
			System.out.println(" What row do you want to add your token");
			int row = Integer.parseInt(scan.nextLine());
			
			System.out.println(" What column do you want to add your token");
			int col = Integer.parseInt(scan.nextLine());
			
			if(player == 1){
				playerToken = Tile.X;
			} else{
				playerToken = Tile.O;
			}
			
			boolean tokenPlaced = board.placeToken(row, col, playerToken);
			
			//While the token is invalid ask for a new input
			while(!tokenPlaced){
				System.out.println(" Enter a valid row to add your token");
				row = Integer.parseInt(scan.nextLine());
				
				System.out.println(" Enter a valid column to add your token");
				col = Integer.parseInt(scan.nextLine());
				
				tokenPlaced = board.placeToken(row, col, playerToken);
			}									
			
			if(board.checkWinning(playerToken) == true){
				System.out.println(board);
				System.out.println("	---------------------------- ");
				System.out.println("	| Congratulations player " + player + " |");
				System.out.println("	---------------------------- ");
				gameOver = true;
			} else if(board.checkFull() == true){
				System.out.println(board);
				System.out.println("	    --------------- ");
				System.out.println("	    | It's a tie! |");
				System.out.println("	    --------------- ");
				gameOver = true;
			} else{
				player++;
				if(player > 2){
					player = 1;
				}
				System.out.println("\033[H\033[2J");
			}
			
		}
    }
}