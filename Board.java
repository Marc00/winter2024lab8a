public class Board{
	private Tile[][] grid;
	private String[][] gridColours;
	private final int SIZE = 3;
	
	//Constructor
	Board(){
		this.grid = new Tile[this.SIZE][this.SIZE];
		this.gridColours = new String[this.SIZE][this.SIZE];
		
		for(int i = 0; i < this.SIZE; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				this.grid[i][j] = Tile.BLANK;
				this.gridColours[i][j] = "\u001B[37m";
			}
		}
	}
	
	public String toString(){
		// 0 1 2 3 adds the column number
		String output = "		0 1 2 3 \n";
		
		for(int i = 0; i < this.SIZE; i++){
			// i + 1 adds the row number
			output += "		"+ (i + 1) + " ";
			for(int j = 0; j < this.SIZE; j++){
				output += this.gridColours[i][j] + this.grid[i][j].getName() + "\u001B[37m ";
			}
			output += "\n";
		}
		return output;		
	}
	
	//Checks if the token can be placed and places it if possible
	public boolean placeToken(int row, int col, Tile playerToken){
		if(row >= 1 && row <= this.SIZE && col >= 1 && col <= this.SIZE && this.grid[row-1][col-1] == Tile.BLANK){
			this.grid[row-1][col-1] = playerToken;
			return true;			
		}
		return false;
	}
	
	public boolean checkFull(){
		for(int i = 0; i < this.SIZE; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				if(this.grid[i][j] == Tile.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkWinningHorizontal(Tile playerToken){
		int counter = 0;
		for(int i = 0; i < this.SIZE; i++){
			if(counter != 3){
				counter = 0;
				for(int j = 0; j < this.SIZE; j++){
					if(this.grid[i][j] == playerToken){
						counter++;			
						this.gridColours[i][j] = "\u001B[32m";
					}						
				}
			}				
		}		
		
		//Resets to white if there is no winning row
		if(counter != 3){
			for(int i = 0; i < this.SIZE; i++){			
				for(int j = 0; j < this.SIZE; j++){							
					this.gridColours[i][j] = "\u001B[37m";
				}							
			}		
		}
		
		return counter == 3;
	}
	
	private boolean checkWinningVertical(Tile playerToken){
		int counter = 0;
		for(int i = 0; i < this.SIZE; i++){
			if(counter != 3){
				counter = 0;
				for(int j = 0; j < this.SIZE; j++){
					if(this.grid[j][i] == playerToken){
						counter++;			
						this.gridColours[i][j] = "\u001B[32m";
					}
						
				}
			}	
		}		
		
		//Resets to white if there is no winning column
		if(counter != 3){
			for(int i = 0; i < this.SIZE; i++){			
				for(int j = 0; j < this.SIZE; j++){							
					this.gridColours[i][j] = "\u001B[37m";
				}							
			}		
		}
		
		return counter == 3;
	}
	
	private boolean checkWinningDiagonal(Tile playerToken){		
		int counter = 0;
		//Top left to bottom right
		for(int i = 0; i < this.SIZE; i++){
			if(this.grid[i][i] == playerToken){
				counter++;	
				this.gridColours[i][i] = "\u001B[32m";
			}											
		}
		
		//Resets to white if this diagonal is not winning 
		if(counter != 3){
			for(int i = 0; i < this.SIZE; i++){			
				for(int j = 0; j < this.SIZE; j++){							
					this.gridColours[i][j] = "\u001B[37m";
				}							
			}		
		}
				
		//Top right to bottom left
		if(counter != 3){
			counter = 0;
			for(int i = 0; i < this.SIZE; i++){
				if(this.grid[this.SIZE - 1 - i][i] == playerToken){
					counter++;
					this.gridColours[this.SIZE - 1 - i][i] = "\u001B[32m";	
				}
													
			}	
		}				

		//Resets to white if there is no diagnonal winning 
		if(counter != 3){
			for(int i = 0; i < this.SIZE; i++){			
				for(int j = 0; j < this.SIZE; j++){							
					this.gridColours[i][j] = "\u001B[37m";
				}							
			}		
		}
		
		return counter == 3;
	}
	
	public boolean checkWinning(Tile playerToken){
		return checkWinningHorizontal(playerToken) || checkWinningVertical(playerToken) || checkWinningDiagonal(playerToken);
	}
}